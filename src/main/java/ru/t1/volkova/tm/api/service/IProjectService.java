/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ru.t1.volkova.tm.api.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.volkova.tm.dto.model.ProjectDTO;


public interface IProjectService {

    @Nullable
    @Transactional
    ProjectDTO create(@Nullable ProjectDTO project);

    @Transactional
    void deleteById(@Nullable String id);

    @NotNull
    List<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findById(@Nullable String id);

    @Transactional
    void removeAll();

    @Transactional
    void saveAll(@NotNull List<ProjectDTO> projects);

    @Nullable
    @Transactional
    ProjectDTO updateById(@Nullable String id);

    @Nullable
    @Transactional
    ProjectDTO update(@Nullable ProjectDTO project);

}
