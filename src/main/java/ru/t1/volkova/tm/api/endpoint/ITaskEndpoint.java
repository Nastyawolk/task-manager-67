package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint {

    @GetMapping("/{id}")
    @WebMethod
    @Nullable
    TaskDTO get(@PathVariable("id") @NotNull String id);

    @PostMapping
    @WebMethod
    void post(@RequestBody @NotNull TaskDTO task);

    @PutMapping
    @WebMethod
    void put(@RequestBody @NotNull TaskDTO task);

    @DeleteMapping("/{id}")
    @WebMethod
    void delete(@PathVariable("id") @NotNull String id);

}
