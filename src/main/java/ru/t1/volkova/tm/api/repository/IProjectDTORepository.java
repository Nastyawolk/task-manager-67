package ru.t1.volkova.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
