package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint {

    @GetMapping("/{id}")
    @WebMethod
    @Nullable
    ProjectDTO get(@PathVariable("id") @NotNull String id);

    @PostMapping
    @WebMethod
    void post(@RequestBody @NotNull ProjectDTO project);

    @PutMapping
    @WebMethod
    void put(@RequestBody @NotNull ProjectDTO project);

    @DeleteMapping("/{id}")
    @WebMethod
    void delete(@PathVariable("id") @NotNull String id);

}
