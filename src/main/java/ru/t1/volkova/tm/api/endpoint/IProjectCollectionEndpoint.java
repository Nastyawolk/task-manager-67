package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectCollectionEndpoint {

    @GetMapping
    @NotNull
    @WebMethod
    List<ProjectDTO> get();

    @PostMapping
    @WebMethod
    void post(@RequestBody @NotNull List<ProjectDTO> projects);

    @PutMapping
    @WebMethod
    void put(@RequestBody @NotNull List<ProjectDTO> projects);

    @DeleteMapping
    @WebMethod
    void delete();

}
