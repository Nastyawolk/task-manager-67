package ru.t1.volkova.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.List;

public class ProjectRestEndpointClient {

    @NotNull
    private final String BASE_URL;

    @NotNull
    private final RestTemplate template;

    @NotNull
    private final HttpHeaders headers;

    public ProjectRestEndpointClient(
            @NotNull final String BASE_URL,
            @NotNull final RestTemplate template,
            @NotNull final HttpHeaders headers
    ) {
        this.BASE_URL = BASE_URL;
        this.template = template;
        this.headers = headers;
    }

    // PROJECT METHODS

    @Nullable
    public ProjectDTO findById(@NotNull final String id) {
        return template.getForObject(BASE_URL + "/{id}", ProjectDTO.class, id);
    }

    @Nullable
    public ProjectDTO save(@NotNull final ProjectDTO project) {
        final HttpEntity entity = new HttpEntity(project, headers);
        return template.postForObject(BASE_URL, entity, ProjectDTO.class);
    }

    public void update(@NotNull final ProjectDTO project) {
        final HttpEntity entity = new HttpEntity(project, headers);
        template.put(BASE_URL, entity, ProjectDTO.class);
    }

    public void deleteById(@NotNull final String id) {
        template.delete(BASE_URL + "/{id}", id);
    }

    // PROJECT COLLECTION METHODS

    @Nullable
    public List<ProjectDTO> findAll() {
        ResponseEntity<List<ProjectDTO>> projectResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
        return projectResponse.getBody();
    }

    public void saveAll(@NotNull final List<ProjectDTO> projects) {
        final HttpEntity entity = new HttpEntity(projects, headers);
        ResponseEntity<List<ProjectDTO>> projectResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.POST, entity, new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
    }

    public void updateAll(@NotNull final List<ProjectDTO> projects) {
        final HttpEntity entity = new HttpEntity(projects, headers);
        ResponseEntity<List<ProjectDTO>> projectResponse =
                template.exchange(BASE_URL + "s",
                        HttpMethod.PUT, entity, new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
    }

    public void deleteAll() {
        template.delete(BASE_URL + "s");
    }

}
