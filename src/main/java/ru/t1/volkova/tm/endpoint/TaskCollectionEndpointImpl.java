package ru.t1.volkova.tm.endpoint;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.volkova.tm.api.endpoint.ITaskCollectionEndpoint;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.ITaskCollectionEndpoint")
public class TaskCollectionEndpointImpl implements ITaskCollectionEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @WebMethod
    @GetMapping()
    public @NotNull List<TaskDTO> get() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping
    public void post(@RequestBody @NotNull List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @WebMethod
    @PutMapping
    public void put(@RequestBody @NotNull List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping()
    public void delete() {
        taskService.removeAll();
    }

}
